package com.andreservidoni.wundertest.view.cars

import com.andreservidoni.wundertest.data.WunderNetwork
import com.andreservidoni.wundertest.data.database.WunderDatabase
import com.andreservidoni.wundertest.models.Car
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import timber.log.Timber
import javax.inject.Inject

class CarsRepository @Inject constructor(private val network: WunderNetwork,
                                         private val database: WunderDatabase) {

    fun fetchLocal(): Single<List<Car>> {
        return database.carDao().getAll().flatMap {
            if(it.isEmpty()) {
                Timber.d("Load from cloud storage!")
                fetchAPI() }
            else {
                Timber.d("Loaded from local storage!")
                Single.just(it) }
        }
    }

    /// Private methods

    private fun fetchAPI(): Single<List<Car>> {
        return network.requestCarList()
                .flatMapPublisher { Flowable.fromIterable(it.content) }
                .map { Car(it.vin!!, it.address, it.coordinates, it.engineType, it.exterior, it.fuel, it.interior, it.name) }
                .toList()
                .flatMap {
                    Timber.e("Saved into local storage!")
                    Completable.fromAction { database.carDao().insertAll(it) }.andThen(Single.just(it))
                }
    }

}