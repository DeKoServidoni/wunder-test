package com.andreservidoni.wundertest.view.main

import android.arch.lifecycle.LiveData
import android.support.design.widget.BottomNavigationView
import com.andreservidoni.wundertest.R
import com.andreservidoni.wundertest.models.status.MainStatus
import com.andreservidoni.wundertest.view.BaseViewModel
import javax.inject.Inject

class MainViewModel @Inject constructor(): BaseViewModel<MainStatus>() {

    override fun dataChannel(): LiveData<MainStatus> = data

    fun getNavigationListener() = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {

            R.id.navigation_home -> {
                data.value = MainStatus.ShowList
                return@OnNavigationItemSelectedListener true
            }

            R.id.navigation_dashboard -> {
                data.value = MainStatus.ShowMap
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    fun loadInitialFragment() {
        data.value = MainStatus.ShowList
    }

}