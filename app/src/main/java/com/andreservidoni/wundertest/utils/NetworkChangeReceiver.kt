package com.andreservidoni.wundertest.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import io.reactivex.processors.PublishProcessor

class NetworkChangeReceiver constructor(private val context: Context?, private val publisher: PublishProcessor<Boolean>): BroadcastReceiver() {

    private var filter = IntentFilter()

    init {
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        publisher.onNext(verifyConnection())
    }

    fun getFilter(): IntentFilter = filter

    /// Private methods

    private fun verifyConnection(): Boolean {
        val connectivityManager = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return connectivityManager.activeNetworkInfo != null
    }
}