package com.andreservidoni.wundertest.data.database.converters

import android.arch.persistence.room.TypeConverter

class DoubleConverter {

    @TypeConverter
    fun fromString(value: String): ArrayList<Double> {
        val arrayList = ArrayList<Double>()

        val splitValue = value.split(",")

        for (strNumber in splitValue) {
            if (strNumber.isNotEmpty())
                arrayList.add(strNumber.toDouble())
        }
        return arrayList
    }

    @TypeConverter
    fun toString(value: ArrayList<Double>): String {

        var final = ""

        for ((index, item) in value.withIndex()) {

            final += item.toString()

            if ((index - 1) != value.size) {
                final += ","
            }

        }
        return final
    }
}