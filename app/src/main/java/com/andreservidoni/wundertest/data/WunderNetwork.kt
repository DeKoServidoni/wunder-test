package com.andreservidoni.wundertest.data

import com.andreservidoni.wundertest.models.network.WunderResponse
import io.reactivex.Single
import retrofit2.http.GET

interface WunderNetwork {

    @GET("wunderbucket/locations.json")
    fun requestCarList(): Single<WunderResponse>
}