package com.andreservidoni.wundertest.utils

import android.databinding.BindingAdapter
import android.support.design.widget.BottomNavigationView
import android.support.v4.widget.SwipeRefreshLayout
import android.view.View
import android.widget.TextView
import java.util.*

object BindAdapters {

    @JvmStatic
    @BindingAdapter("navigation")
    fun setNavigation(bottomNavigationView: BottomNavigationView, listener: BottomNavigationView.OnNavigationItemSelectedListener) {
        bottomNavigationView.setOnNavigationItemSelectedListener(listener)
    }

    @JvmStatic
    @BindingAdapter("value", "resource")
    fun setCompostText(textView: TextView, value: String, resource: String) {
        textView.text = String.format(Locale.US, "%s: %s", resource, value)
    }

    @JvmStatic
    @BindingAdapter("isToRefresh")
    fun setRefresh(swipe: SwipeRefreshLayout, visibility: Int) {
        swipe.isRefreshing = (visibility == View.VISIBLE)
    }
}