package com.andreservidoni.wundertest.di.modules

import com.andreservidoni.wundertest.view.cars.CarsFragment
import com.andreservidoni.wundertest.view.main.MainActivity
import com.andreservidoni.wundertest.view.maps.MapsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityBuildersModule {

    @ContributesAndroidInjector()
    abstract fun bindMainActivity(): MainActivity

    // fragments

    @ContributesAndroidInjector
    internal abstract fun contributeHomeFragment(): CarsFragment

    @ContributesAndroidInjector
    internal abstract fun contributeMapsFragment(): MapsFragment
}