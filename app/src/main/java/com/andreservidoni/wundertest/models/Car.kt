package com.andreservidoni.wundertest.models

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull

@Entity
data class Car(
        @NonNull
        @PrimaryKey
        @ColumnInfo(name ="vin")
        var vin: String = "",

        @ColumnInfo(name = "address")
        var address: String? = "",

        @ColumnInfo(name = "coordinates")
        var coordinates: ArrayList<Double> = ArrayList(),

        @ColumnInfo(name = "engineType")
        var engineType: String? = "",

        @ColumnInfo(name = "exterior")
        var exterior: String? = "",

        @ColumnInfo(name = "fuel")
        var fuel: Int? = 0,

        @ColumnInfo(name = "interior")
        var interior: String? = "",

        @ColumnInfo(name = "name")
        var name: String? = ""
)