package com.andreservidoni.wundertest.di.modules

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.andreservidoni.wundertest.di.AppViewModelFactory
import com.andreservidoni.wundertest.di.ViewModelKey
import com.andreservidoni.wundertest.view.cars.viewmodels.CarsViewModel
import com.andreservidoni.wundertest.view.main.MainViewModel
import com.andreservidoni.wundertest.view.maps.MapsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: AppViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun mainViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CarsViewModel::class)
    abstract fun homeViewModel(viewModel: CarsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MapsViewModel::class)
    abstract fun mapsViewModel(viewModel: MapsViewModel): ViewModel
}