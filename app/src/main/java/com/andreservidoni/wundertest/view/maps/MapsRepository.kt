package com.andreservidoni.wundertest.view.maps

import com.andreservidoni.wundertest.data.database.WunderDatabase
import com.andreservidoni.wundertest.models.Pin
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

class MapsRepository @Inject constructor(private val database: WunderDatabase) {

    fun fetchPins(): Single<List<Pin>> {
        return database.carDao().getAll()
                .flatMapPublisher { Flowable.fromIterable(it) }
                .map { Pin(it.fuel, it.name, it.coordinates[1], it.coordinates[0]) }
                .toList()
    }
}