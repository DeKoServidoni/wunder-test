package com.andreservidoni.wundertest.di.modules

import android.arch.persistence.room.Room
import android.content.Context
import com.andreservidoni.wundertest.BaseApp
import com.andreservidoni.wundertest.BuildConfig
import com.andreservidoni.wundertest.data.WunderNetwork
import com.andreservidoni.wundertest.data.database.WunderDatabase
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    fun provideContext(application: BaseApp): Context {
        return application.applicationContext
    }

    @Singleton
    @Provides
    fun provideGson(): Gson = GsonBuilder().create()

    @Singleton
    @Provides
    fun provideNetwork(gson: Gson): WunderNetwork {
        return Retrofit.Builder().baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build()
                .create(WunderNetwork::class.java)
    }

    @Singleton
    @Provides
    fun provideDatabase(context: Context): WunderDatabase {
        return Room.databaseBuilder(context, WunderDatabase::class.java, "database-wunder").build()
    }
}