package com.andreservidoni.wundertest.models.status

sealed class MainStatus {
    object ShowList: MainStatus()
    object ShowMap: MainStatus()
}