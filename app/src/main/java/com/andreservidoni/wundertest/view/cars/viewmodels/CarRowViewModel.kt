package com.andreservidoni.wundertest.view.cars.viewmodels

import android.databinding.ObservableInt
import android.view.View
import com.andreservidoni.wundertest.models.Car
import java.util.*

class CarRowViewModel(private val car: Car) {

    val contentVisibility = ObservableInt(View.GONE)

    fun onClick() {
        contentVisibility.set( if(contentVisibility.get() == View.GONE)
            View.VISIBLE else View.GONE )
    }

    fun getName(): String? = String.format(Locale.US, "%s (%s)", car.name, car.vin)

    fun getAddress(): String? = car.address

    fun getEngine(): String? = car.engineType

    fun getInterior(): String? = car.interior

    fun getExterior(): String? = car.exterior

    fun getFuel(): String? = String.format(Locale.US, "%s", car.fuel)
}