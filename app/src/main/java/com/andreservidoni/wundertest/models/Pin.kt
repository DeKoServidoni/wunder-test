package com.andreservidoni.wundertest.models

data class Pin(
        val fuel: Int? = 0,
        val name: String? = "",
        val lat: Double? = 0.0,
        val lng: Double? = 0.0
)