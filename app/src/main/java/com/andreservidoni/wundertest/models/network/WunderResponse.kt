package com.andreservidoni.wundertest.models.network

import com.google.gson.annotations.SerializedName

data class WunderResponse(
        @SerializedName("placemarks")
        val content: ArrayList<WunderCar> = ArrayList()
)

data class WunderCar(
        @SerializedName("address")
        val address: String? = "",

        @SerializedName("coordinates")
        val coordinates: ArrayList<Double> = ArrayList(),

        @SerializedName("engineType")
        val engineType: String? = "",

        @SerializedName("exterior")
        val exterior: String? = "",

        @SerializedName("fuel")
        val fuel: Int? = 0,

        @SerializedName("interior")
        val interior: String? = "",

        @SerializedName("name")
        val name: String? = "",

        @SerializedName("vin")
        val vin: String? = ""
)
