package com.andreservidoni.wundertest.view.cars

import com.andreservidoni.wundertest.R
import com.andreservidoni.wundertest.databinding.RowCarBinding
import com.andreservidoni.wundertest.models.Car
import com.andreservidoni.wundertest.view.BaseAdapter
import com.andreservidoni.wundertest.view.cars.viewmodels.CarRowViewModel

class CarsAdapter: BaseAdapter<Car, RowCarBinding>(R.layout.row_car) {

    override fun areItemsTheSame(oldItem: Car, newItem: Car): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Car, newItem: Car): Boolean {
        return oldItem.vin == newItem.vin
    }

    override fun bind(holder: DataBindViewHolder<RowCarBinding>, position: Int) {
        val viewModel = CarRowViewModel(items[position])
        holder.binding.viewModel = viewModel
        holder.binding.executePendingBindings()
    }
}