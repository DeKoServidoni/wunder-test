package com.andreservidoni.wundertest.data.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.andreservidoni.wundertest.data.database.converters.DoubleConverter
import com.andreservidoni.wundertest.data.database.dao.CarDAO
import com.andreservidoni.wundertest.models.Car

@Database(entities = [Car::class], version = 1)
@TypeConverters(DoubleConverter::class)
abstract class WunderDatabase : RoomDatabase() {
    abstract fun carDao(): CarDAO
}