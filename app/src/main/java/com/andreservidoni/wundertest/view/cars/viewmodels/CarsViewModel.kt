package com.andreservidoni.wundertest.view.cars.viewmodels

import android.arch.lifecycle.LiveData
import android.databinding.Bindable
import android.view.View
import com.andreservidoni.wundertest.BR
import com.andreservidoni.wundertest.models.status.CarsStatus
import com.andreservidoni.wundertest.view.BaseViewModel
import com.andreservidoni.wundertest.view.cars.CarsRepository
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CarsViewModel @Inject constructor(private val repository: CarsRepository): BaseViewModel<CarsStatus>() {

    private var contentVisibility = View.GONE
    private var progressVisibility = View.VISIBLE

    override fun dataChannel(): LiveData<CarsStatus> = data

    fun getCars() {
        if(hasConnection) {
            showLoading(true)

            compositeDisposable.add(repository.fetchLocal()
                    .subscribeOn(Schedulers.io())
                    .subscribe({
                        showLoading(false)
                        data.postValue(CarsStatus.Success(it))
                    }, {
                        showLoading(false)
                        data.postValue(CarsStatus.Error(it.localizedMessage))
                    }))
        }
    }

    // Bindable methods

    @Bindable
    fun getContentVisibility(): Int = contentVisibility

    @Bindable
    fun getProgressVisibility(): Int = progressVisibility

    // Private methods

    private fun showLoading(show: Boolean) {
        contentVisibility = if(show) View.GONE else View.VISIBLE
        progressVisibility = if(show) View.VISIBLE else View.GONE

        notifyPropertyChanged(BR.contentVisibility)
        notifyPropertyChanged(BR.progressVisibility)
    }
}