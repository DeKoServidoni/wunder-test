package com.andreservidoni.wundertest.data.database.dao

import android.arch.persistence.room.*
import com.andreservidoni.wundertest.models.Car
import io.reactivex.Single


@Dao
interface CarDAO {

    @Query("SELECT * FROM Car")
    fun getAll(): Single<List<Car>>

    @Transaction
    @Insert
    fun insertAll(cars:List<Car>)

    @Delete
    fun delete(car: Car)
}