package com.andreservidoni.wundertest.view.main

import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import com.andreservidoni.wundertest.R
import com.andreservidoni.wundertest.databinding.ActivityMainBinding
import com.andreservidoni.wundertest.models.status.MainStatus
import com.andreservidoni.wundertest.view.cars.CarsFragment
import com.andreservidoni.wundertest.view.maps.MapsFragment
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val REQUEST_PERMISSION = 1

    private val viewModel by lazy { ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // verify location permission
        if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),REQUEST_PERMISSION)
        }

        // register
        registerData()

        // bind UI

        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        binding.viewModel = viewModel

        viewModel.loadInitialFragment()
    }

    // Private methods

    private fun registerData() {

        viewModel.dataChannel().observe(this, Observer { status ->
            status?.let {

                when(it) {
                    is MainStatus.ShowList -> {
                        supportFragmentManager.beginTransaction()
                                .replace(R.id.home_container, CarsFragment.newInstance()).commit()
                    }

                    is MainStatus.ShowMap -> {
                        supportFragmentManager.beginTransaction()
                                .replace(R.id.home_container, MapsFragment.newInstance()).commit()
                    }
                }
            }
        })
    }
}
