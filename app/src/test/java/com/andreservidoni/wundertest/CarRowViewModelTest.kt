package com.andreservidoni.wundertest

import com.andreservidoni.wundertest.models.Car
import com.andreservidoni.wundertest.view.cars.viewmodels.CarRowViewModel
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class CarRowViewModelTest {

    private lateinit var viewModel: CarRowViewModel

    @Before
    fun setUp() {
        val car = Car("WME4513341K565439", "Lesserstraße 170, 22049 Hamburg", ArrayList(), "CE",
                "UNACCEPTABLE", 42, "UNACCEPTABLE", "HH-GO8522")

        viewModel = CarRowViewModel(car)
    }

    @Test
    fun shouldShowCarData() {
        assertEquals("Invalid name!", "HH-GO8522 (WME4513341K565439)", viewModel.getName())
        assertEquals("Invalid address!", "Lesserstraße 170, 22049 Hamburg", viewModel.getAddress())
        assertEquals("Invalid engine!", "CE", viewModel.getEngine())
        assertEquals("Invalid interior!", "UNACCEPTABLE", viewModel.getInterior())
        assertEquals("Invalid exterior!", "UNACCEPTABLE", viewModel.getExterior())
        assertEquals("Invalid fuel!", "42", viewModel.getFuel())
    }
}