package com.andreservidoni.wundertest.models.status

import com.andreservidoni.wundertest.models.Pin

sealed class MapsStatus {
    data class Success(val content: List<Pin>?): MapsStatus()
    data class Error(val error: String?): MapsStatus()
}