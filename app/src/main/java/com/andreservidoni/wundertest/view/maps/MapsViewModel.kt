package com.andreservidoni.wundertest.view.maps

import android.arch.lifecycle.LiveData
import android.databinding.Bindable
import android.location.Location
import android.view.View
import com.andreservidoni.wundertest.BR
import com.andreservidoni.wundertest.models.Pin
import com.andreservidoni.wundertest.models.status.MapsStatus
import com.andreservidoni.wundertest.utils.MapManager
import com.andreservidoni.wundertest.view.BaseViewModel
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MapsViewModel @Inject constructor(private val repository: MapsRepository)
    : BaseViewModel<MapsStatus>() {

    private var progressVisibility = View.VISIBLE
    private var infoVisibility = View.GONE
    private var carName: String? = ""
    private var mapManager: MapManager? = null

    override fun dataChannel(): LiveData<MapsStatus> = data

    fun getPins() {
        showLoading(true)

        compositeDisposable.add(repository.fetchPins()
                .subscribeOn(Schedulers.io())
                .subscribe({
                    data.postValue(MapsStatus.Success(it))
                } , {
                    showLoading(false)
                    data.postValue(MapsStatus.Error(it.localizedMessage))
                }))
    }

    fun showInfo(name: String) {
        carName = name
        infoVisibility = View.VISIBLE
        notifyPropertyChanged(BR.infoVisibility)
        notifyPropertyChanged(BR.carName)
    }

    fun hideInfo() {
        infoVisibility = View.GONE
        notifyPropertyChanged(BR.infoVisibility)
    }

    // Maps methods

    fun initializeMap(map: GoogleMap?) {
        map?.let {
            mapManager = MapManager(it)
        }
    }

    fun updateUserLocation(location: Location) {
        mapManager?.placePin(location)
    }

    fun placePin(marker: Marker) {
        mapManager?.placePin(marker)
    }

    fun replacePins() {
        mapManager?.placePins()
    }

    fun placeCars(list: List<Pin>) {
        mapManager?.buildMarkers(list)
        showLoading(false)
    }

    // Bindable methods

    @Bindable
    fun getProgressVisibility(): Int = progressVisibility

    @Bindable
    fun getInfoVisibility(): Int = infoVisibility

    @Bindable
    fun getCarName(): String? = carName

    // Private methods

    private fun showLoading(show: Boolean) {
        progressVisibility = if(show) View.VISIBLE else View.GONE
        notifyPropertyChanged(BR.progressVisibility)
    }
}