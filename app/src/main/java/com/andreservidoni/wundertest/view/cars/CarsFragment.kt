package com.andreservidoni.wundertest.view.cars

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.andreservidoni.wundertest.R
import com.andreservidoni.wundertest.databinding.FragmentCarsBinding
import com.andreservidoni.wundertest.models.status.CarsStatus
import com.andreservidoni.wundertest.view.BaseFragment
import com.andreservidoni.wundertest.view.cars.viewmodels.CarsViewModel
import io.reactivex.processors.PublishProcessor
import kotlinx.android.synthetic.main.fragment_cars.*
import javax.inject.Inject

class CarsFragment: BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy { ViewModelProviders.of(this, viewModelFactory).get(CarsViewModel::class.java) }

    private lateinit var homeAdapter: CarsAdapter
    private lateinit var binding: FragmentCarsBinding

    companion object {

        fun newInstance(): CarsFragment {
            return CarsFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cars, container, false)
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupNetworkObserver()

        // register swipe
        binding.swipe.setOnRefreshListener {
            viewModel.getCars()
        }

        // set up UI
        homeAdapter = CarsAdapter()

        with(content_list) {
            this.layoutManager = LinearLayoutManager(context)
            this.setHasFixedSize(true)
            this.adapter = homeAdapter
        }

        // register channel
        viewModel.dataChannel().observe(this, Observer {status ->
            status?.let {

                when(it) {
                    is CarsStatus.Success -> {
                        it.content?.let { content ->
                            homeAdapter.update(content)
                        }
                    }

                    is CarsStatus.Error -> {
                        showSnack(binding.root, it.error, null)
                    }
                }
            }
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.getCars()
    }

    // Private methods

    private fun setupNetworkObserver() {
        networkListener = PublishProcessor.create<Boolean>()

        networkListener?.subscribe {
            viewModel.hasConnection = it
            viewModel.getCars()

            if(it) dismissSnack() else showSnack(binding.root, getString(R.string.network_error), null)
        }

        networkListener?.let {
            listenToNetworkChanges(it)
        }
    }

}