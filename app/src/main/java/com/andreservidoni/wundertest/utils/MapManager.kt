package com.andreservidoni.wundertest.utils

import android.location.Location
import com.andreservidoni.wundertest.R
import com.andreservidoni.wundertest.models.Pin
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import timber.log.Timber

class MapManager(private val map: GoogleMap) {

    private val markers = ArrayList<Marker>()
    private var current: LatLng? = null
    private var coordinatesBuilder = LatLngBounds.builder()

    init {
        map.uiSettings.isZoomControlsEnabled = true
        map.uiSettings.isCompassEnabled = true
        map.uiSettings.isZoomGesturesEnabled = false
    }

    fun buildMarkers(list: List<Pin>) {
        for(item in list){
            val coordinates = LatLng(item.lat!!, item.lng!!)
            Timber.e(item.name)
            markers.add(addMarker(map, coordinates, item.name!!)!!)
            coordinatesBuilder.include(coordinates)
        }

        current?.let {
            zoomToPin(it)
        } ?: run {
            zoomToBounds()
        }
    }

    fun placePin(location: Location) {
        current = LatLng(location.latitude, location.longitude)
        map.addMarker(MarkerOptions().position(current!!))
    }

    fun placePin(marker: Marker) {
        map.clear()
        addMarker(map, marker.position, marker.title)
        zoomToPin(marker.position)
    }

    fun placePins() {
        map.clear()
        for(marker in markers) {
            addMarker(map, marker.position, marker.title)
        }
        zoomToBounds()
    }

    // Private methods

    private fun addMarker(map: GoogleMap?, coordinates: LatLng, title: String?)
            : Marker? = map?.addMarker(MarkerOptions()
            .position(coordinates)
            .title(title)
            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_pin_black_18dp)))

    private fun zoomToPin(latLng: LatLng) {
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18.0f))
    }

    private fun zoomToBounds() {
        map.animateCamera(CameraUpdateFactory.newLatLngBounds(coordinatesBuilder.build(), 50))
    }
}