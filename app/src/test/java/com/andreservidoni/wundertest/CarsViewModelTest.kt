package com.andreservidoni.wundertest

import android.arch.lifecycle.Observer
import com.andreservidoni.wundertest.data.WunderNetwork
import com.andreservidoni.wundertest.data.database.WunderDatabase
import com.andreservidoni.wundertest.data.database.dao.CarDAO
import com.andreservidoni.wundertest.models.network.WunderCar
import com.andreservidoni.wundertest.models.network.WunderResponse
import com.andreservidoni.wundertest.models.status.CarsStatus
import com.andreservidoni.wundertest.view.cars.CarsRepository
import com.andreservidoni.wundertest.view.cars.viewmodels.CarsViewModel
import com.google.gson.Gson
import io.reactivex.Single
import okhttp3.mockwebserver.MockResponse
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class CarsViewModelTest: BaseTest() {

    private lateinit var viewModel: CarsViewModel

    @Mock
    private lateinit var database: WunderDatabase

    @Mock
    private lateinit var dao: CarDAO

    @Mock
    private lateinit var observer: Observer<CarsStatus>

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        val network = retroFit.create(WunderNetwork::class.java)
        val repository = CarsRepository(network, database)

        Mockito.`when`(database.carDao()).thenReturn(dao)
        Mockito.`when`(database.carDao().getAll()).thenReturn(Single.just(ArrayList()))

        viewModel = CarsViewModel(repository)
        viewModel.dataChannel().observeForever(observer)
    }

    @Test
    fun shouldFetchSuccess() {
        val coordinates = ArrayList<Double>()
        val car = WunderCar("Lesserstraße 170, 22049 Hamburg", coordinates,
                "CE", "UNACCEPTABLE", 42, "UNACCEPTABLE",
                "HH-GO8522", "WME4513341K565439")
        val list = ArrayList<WunderCar>()
        list.add(car)
        val response = WunderResponse(list)

        mockServerResponse(200, response)

        viewModel.getCars()

        Mockito.verify(observer).onChanged(ArgumentMatchers.any<CarsStatus.Success>())
    }

    @Test
    fun shouldFetchFailed() {
        mockServerResponse(500, WunderResponse())

        viewModel.getCars()

        Mockito.verify(observer).onChanged(ArgumentMatchers.any<CarsStatus.Error>())
    }

    // Private methods

    private fun mockServerResponse(responseCode: Int, mockResponse: WunderResponse) {
        val mockResult = Gson().toJson(mockResponse)
        server.enqueue(MockResponse().setResponseCode(responseCode).setBody(mockResult))
    }
}