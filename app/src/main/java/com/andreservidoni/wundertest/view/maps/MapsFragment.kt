package com.andreservidoni.wundertest.view.maps

import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.andreservidoni.wundertest.R
import com.andreservidoni.wundertest.databinding.FragmentMapsBinding
import com.andreservidoni.wundertest.models.status.MapsStatus
import com.andreservidoni.wundertest.view.BaseFragment
import com.google.android.gms.location.*
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import javax.inject.Inject


class MapsFragment: BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy { ViewModelProviders.of(this, viewModelFactory).get(MapsViewModel::class.java) }

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var binding: FragmentMapsBinding
    private lateinit var map: GoogleMap

    private var showAll = true

    private val locationRequest = LocationRequest().apply {
        interval = 10000
        fastestInterval = 5000
        priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    companion object {

        fun newInstance(): MapsFragment {
            return MapsFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_maps, container, false)
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync {
            map = it

            setMapClickListeners()
            setMapCurrentLocation()

            viewModel.initializeMap(it)
            viewModel.getPins()
        }

        // register channel
        viewModel.dataChannel().observe(this, Observer {status ->
            status?.let {

                when(it) {
                    is MapsStatus.Success -> {
                        viewModel.placeCars(it.content!!)
                    }

                    is MapsStatus.Error -> {
                        showSnack(binding.root, it.error, null)
                    }
                }
            }
        })
    }

    /// Private methods

    private fun setMapClickListeners() {
        map.setOnMarkerClickListener {

            if (showAll) {
                showAll = false
                viewModel.placePin(it)
                viewModel.showInfo(it.title)
            } else {
                showAll = true
                viewModel.hideInfo()
                viewModel.replacePins()
            }

            true
        }
    }

    private fun setMapCurrentLocation() {
        if (ContextCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            fusedLocationClient = LocationServices.getFusedLocationProviderClient(context!!)
            fusedLocationClient.requestLocationUpdates(locationRequest, object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult?) {
                    locationResult ?: return
                    for (location in locationResult.locations){
                        viewModel.updateUserLocation(location)
                    }
                }
            }, null)

            map.isMyLocationEnabled = true
            map.uiSettings.isMyLocationButtonEnabled = true
        }
    }
}