package com.andreservidoni.wundertest.models.status

import com.andreservidoni.wundertest.models.Car

sealed class CarsStatus {
    data class Success(val content: List<Car>?): CarsStatus()
    data class Error(val error: String?): CarsStatus()
}